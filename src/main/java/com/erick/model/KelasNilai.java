package com.erick.model;

import lombok.Getter;
import lombok.Setter;



public class KelasNilai {
    private String kelas;
    private String [] nilai;

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String[] getNilai() {
        return nilai;
    }

    public void setNilai(String[] nilai) {
        this.nilai = nilai;
    }
}

