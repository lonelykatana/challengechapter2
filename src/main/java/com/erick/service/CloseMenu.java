package com.erick.service;

public class CloseMenu extends Menu {
    @Override
    public void showMenu() {
        MainMenu main = new MainMenu();
        System.out.println("File telah diproses! Tentukan pilihan anda: \n1. Kembali ke menu utama \n0.Exit");
        switch (input()) {
            case 0:
                System.out.println("Program sedang ditutup");
                System.exit(0);
                break;
            case 1:
                main.showMenu();
                break;
            default:
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.showMenu();
                break;
        }
    }
}
