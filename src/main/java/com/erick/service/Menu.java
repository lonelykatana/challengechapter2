package com.erick.service;

import java.util.Scanner;

public abstract class Menu {
    private Scanner scanner=new Scanner(System.in);

    public int input(){
        System.out.println("Masukkan pilihan anda : ");
        return scanner.nextInt();
    }
    public abstract void showMenu();
}
