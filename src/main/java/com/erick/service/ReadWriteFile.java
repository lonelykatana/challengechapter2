package com.erick.service;

public interface ReadWriteFile {
    void olahNilai(String filePath, String filePathOlahNilai, String delimiter);

    void writeFile(String filePathWrite, String filePath, String delimiter);
}
