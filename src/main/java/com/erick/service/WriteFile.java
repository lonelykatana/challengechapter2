package com.erick.service;

import java.io.*;
import java.util.*;

public class WriteFile implements ReadWriteFile {


    @Override
    public void olahNilai(String filePath, String filePatOlahNilai, String delimiter) {
        try {
            File file2 = new File(filePath);
            FileReader fr = new FileReader(file2);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;

            List<Integer> listInt = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimiter);

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }
                }
            }
            File file3 = new File(filePatOlahNilai);
            if (file3.createNewFile()) {
                System.out.println("FIle sudah dibuat");
            }
            FileWriter writer = new FileWriter(file3);
            BufferedWriter bwr = new BufferedWriter(writer);
            Map<Integer, Integer> hMap = freq(listInt);
            Set<Integer> key = hMap.keySet();

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Nilai\t\t\t\t" + "|\t\t" + "Frekuensi" + "\n");

            for (Integer nilai : key) {
                bwr.write(nilai + "\t\t\t\t\t" + "|\t\t" + hMap.get(nilai) + "\n");
            }


            bwr.flush();
            br.close();
            bwr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void writeFile(String filePathWrite, String filePath, String delimiter) {
        try {
            //READ FILE
            File file = new File(filePath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;

            List<Integer> listInt = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimiter);

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }
                }
            }


            //WRITE FILE
            File file1 = new File(filePathWrite);
            if (file1.createNewFile()) {
                System.out.println("File sudah dibuat");
            }
            FileWriter writer = new FileWriter(file1);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Pengolahan Nilai");
            bwr.newLine();
            bwr.write("Mean : " + String.format("%.2f", mean(listInt)));
            bwr.newLine();
            bwr.write("Mean : " + median(listInt));
            bwr.newLine();
            bwr.write("Modus : " + modus(listInt));
            bwr.flush();
            br.close();
            bwr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private double mean(List<Integer> list) {
        return list.stream().mapToDouble(d -> d)
                .average().orElse(0.0);
    }

    private double median(List<Integer> list) {
        Arrays.sort(new List[]{list});
        double median;
        if (list.size() % 2 == 0)
            median = ((double) list.get(list.size() / 2) + (double) list.get(list.size() / 2 - 1)) / 2;
        else
            median = (double) list.get(list.size() / 2);
        return median;
    }

    private int modus(List<Integer> list) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : list) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }

    private Map<Integer, Integer> freq(List<Integer> array) {
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }

}
